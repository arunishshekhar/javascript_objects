const InvertFile = require('../invert')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

console.log(InvertFile.invert(testObject))//normal
console.log(InvertFile.invert({ a: { b: 1}}))//nested object