const file = require('../mapObject')

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

console.log(file.mapObject(testObject,file.modifyingKeyValue))//normal
console.log(file.mapObject(testObject))//no function
console.log(file.mapObject(file.modifyingKeyValue))//no object
console.log(file.mapObject())//empty