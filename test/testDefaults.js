const defaultsFile = require('../defaults')

let iceCream = {flavor: "chocolate"};
let defaulter = {flavor: "vanilla", sprinkles: "lots"}

console.log(defaultsFile.defaults(iceCream,defaulter))//normal
console.log(defaultsFile.defaults(iceCream))//no default
console.log(defaultsFile.defaults(defaulter))//no main obj
console.log(defaultsFile.defaults())//empty